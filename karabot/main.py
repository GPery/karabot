import argparse
import logging
import subprocess
import sys

import telegram
import telegram.ext
import youtube_dl

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

VLC_ARGS = [
    '--play-and-exit',
    '--one-instance',
]


def add(update: telegram.Update, context: telegram.ext.CallbackContext):
    url = update.message.text.split()[1].strip()
    logging.info(f"Downloading {url}")

    with youtube_dl.YoutubeDL({'simulate': True}) as ydl:
        info = ydl.extract_info(url)
        filename = ydl.prepare_filename(info)
        vlc_cli = ['vlc', *VLC_ARGS, filename]
        logging.info(vlc_cli)
        subprocess.Popen(vlc_cli)
    update.message.reply_text("Added to queue!")


def main():
    ap = argparse.ArgumentParser()
    ap.add_argument('-t', '--token', type=argparse.FileType('r'))

    args = ap.parse_args(sys.argv[1:])
    token = args.token.read().strip()

    u = telegram.ext.Updater(token=token)

    download_handler = telegram.ext.CommandHandler('add', add)
    u.dispatcher.add_handler(download_handler)

    logging.info("Polling...")
    u.start_polling()


if __name__ == '__main__':
    main()
